# Changelog

## Version 9 - 2021-11-04
### Changed
- Refactor

## Version 8 - 2021-11-04
### Changed
- Port extension to Gnome Shell 40
- Display _Please wait for the operation to complete_ message on GPU switch
- Display _Logging out_ message on session logout

## Version 7 - 2020-05-19
### Changed
- Gnome session crash fix

## Version 6 - 2020-05-19
### Changed
- Gnome shell 3.36 support
### Added
- _NVidia On-Demand_ menu item

## Version 5 - 2020-01-21
### Changed
- Gnome shell 3.34 support

## Version 4 - 2020-01-21
### Changed
- Gnome shell 3.32 support
- Preferences styling

## Version 3 - 2020-01-21
### Changed
- Gnome shell 3.30 support

## Version 2 - 2019-11-18
### Changed
- Fix _refusing to render service to dead parents_ error
- Get current GPU fix
- Refactor

## Version 1 - 2017-07-31
### Added
- Prime Indicator aggregate menu
