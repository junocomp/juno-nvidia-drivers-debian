SHELL := /bin/bash
# Install Juno-Nvidia-Drivers

DESTDIR=debian/juno-nvidia-drivers

install-core:
	install -dm755 $(DESTDIR)/usr/bin
	install -dm755 $(DESTDIR)/usr/share/juno-nvidia
	install -dm755 $(DESTDIR)/etc/modprobe.d/
	install -dm755 $(DESTDIR)/etc/systemd/system
	install -Dpm 0644 20-nvidiatripleBuffer.conf $(DESTDIR)/etc/X11/xorg.conf.d/20-nvidiatripleBuffer.conf
	install -Dpm 0755 intel-turbo-boost $(DESTDIR)/usr/bin/intel-turbo-boost
	install -Dpm 0644 intel-turbo-boost.service $(DESTDIR)/etc/systemd/system/intel-turbo-boost.service
	cp -R com.leinardi.gwe $(DESTDIR)/usr/share/juno-nvidia
	install -Dpm 0644 com.leinardi.gwe.desktop $(DESTDIR)/usr/share/juno-nvidia/com.leinardi.gwe.desktop

install: install-core

uninstall:
	rm -f $(DESTDIR)/usr/bin/intel-turbo-boost
	rm -f $(DESTDIR)/etc/systemd/system/intel-turbo-boost.service
	rm -f $(DESTDIR)/etc/modprobe.d/blacklist-nouveau.conf
	rm -R $(DESTDIR)/usr/share/juno-nvidia
	rm -f $(DESTDIR)/etc/X11/xorg.conf.d/20-nvidiatripleBuffer.conf